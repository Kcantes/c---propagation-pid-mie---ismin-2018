//Projet d'AlgoProg 2, par Quentin CHEVALIER et Maxime CHIANESE 06/04/2019
//projet.c : code source principal

#include <stdio.h>
#include <stdlib.h>
#include "graphe.h"
//PARAMETRES---------------------------------------------------
int taille = 20;
int duree = 200;
float pInfectesAtStart = 0.1;
float tIncubation = 4;
float pInfection = 0.2;
float pMortalite = 0.2;
float pRetablissement = 0.3;
float pImmun = 0.4;
float pMortVersZombie = 0.2;
float pZmortDef = 0.3;
//-------------------------------------------------------------
//FONCTIONS----------------------------------------------------
int *letsGo(Graphe *graphe);//lance la simulation, renvoie la durée de la simulation, le pic de malade et le jour du pic par un tableau
void infecte(Graphe *graphe, float num);//infecte une certaine proportion num de la population
void propage(Graphe *graphe);//fonction de propagation, établit les changements d'état
void appliquer(Graphe *graphe, char *etat_suivant);//applique les changements d'états aux sommets
void update_successors(Graphe *graphe);//mets à jour l'ensemble du graphe
void Menu(Graphe *graphe, int res[3]);//affiche le menu
void changeParam(int param);//permet de changer les parametres de la simulation
//-------------------------------------------------------------
//MAIN---------------------------------------------------------
void main()
{
    Graphe graphe;
    int res[3];
    Menu(&graphe, res);
}
//-------------------------------------------------------------
//PROGRAMME----------------------------------------------------
//fonctions de propagation--------
int *letsGo(Graphe *graphe)
{
    init_graphe(graphe, taille, pImmun);
    infecte(graphe, pInfectesAtStart);
    int finish = 0;
    int pic = 0;
    int timePic = 0;
    int tFin = duree;
    for (int i = 0; i < duree; i++)
    {
        propage(graphe);
        printf("\n");
        int infc = 0;
        for (int k = 0; k < graphe->nb_sommets; k++)
        {
            if (graphe->successeurs[k]->etat == 'I')
                infc++;
        }
        if (infc > pic)
        {

            pic = infc;
            timePic = i + 1;
        }
        int a = recensement(graphe);
        if (a == 0 && finish == 0)
        {
            finish = 1;
            printf("\n-----> Aucun infecté restant, continuer ? yes(1)/no(0)\n>>");
            int res = 0;
            scanf("%d", &res);
            if (res == 0)
            {
                tFin = i + 1;
                break;
            }
        }
    }
    int *res = (int *)malloc(sizeof(int) * 3);
    res[0] = tFin;
    res[1] = pic;
    res[2] = timePic;
    return (res);
}

void infecte(Graphe *graphe, float num)
{
    int nb = (int)(num * graphe->nb_sommets);
    for (int k = 0; k < nb; k++)
    {
        int rdm = (int)(rand() / (float)RAND_MAX * graphe->nb_sommets);
        while (graphe->successeurs[rdm]->etat == 'I')
        {
            rdm = (int)(rand() / (float)RAND_MAX * graphe->nb_sommets);
        }
        graphe->successeurs[rdm]->etat = 'I';
    }
    update_successors(graphe);
}
void propage(Graphe *graphe)
{
    char *etat_suivant = (char *)malloc(sizeof(char) * graphe->nb_sommets);
    for (int i = 0; i < graphe->nb_sommets; i++)
    {
        etat_suivant[i] = 'N';
    }
    for (int i = 0; i < graphe->nb_sommets; i++)
    {

        cell *current = (cell *)malloc(sizeof(cell));
        current = graphe->successeurs[i];
        if (current->etat == 'S' || current->etat == 'V' || current->etat == 'M')
            continue;
        if (current->etat == 'C')
        {
            current->tm++;
            if (current->tm >= tIncubation)
            {
                etat_suivant[i] = 'I';
            }
        }
        if (current->etat == 'Z')
        {

            cell *succ = current->suivant;
            while (succ != NULL)
            {
                if (succ->etat == 'S')
                {
                    int rdm = rand();
                    if (rdm < pInfection * RAND_MAX)
                    {
                        etat_suivant[taille * succ->x + succ->y] = 'C';
                    }
                }
                succ = succ->suivant;
            }
            int rdm = rand();
            if (rdm < pZmortDef * RAND_MAX)
            {
                etat_suivant[taille * (current->x) + current->y] = 'M';
            }
        }
        if (current->etat == 'I')
        {
            cell *succ = current->suivant;
            while (succ != NULL)
            {
                if (succ->etat == 'S')
                {
                    int rdm = rand();
                    if (rdm < pInfection * RAND_MAX)
                    {
                        etat_suivant[taille * succ->x + succ->y] = 'C';
                    }
                }
                succ = succ->suivant;
            }
            int rdm = rand();
            if (rdm < pRetablissement * RAND_MAX)
            {
                etat_suivant[taille * (current->x) + current->y] = 'V';
            }
            if (rdm > pRetablissement * RAND_MAX && rdm < pRetablissement * RAND_MAX + pMortalite * RAND_MAX)
            {
                etat_suivant[taille * (current->x) + current->y] = 'M';
                rdm = rand();
                if (rdm < pMortVersZombie * RAND_MAX)
                {
                    etat_suivant[taille * current->x + current->y] = 'Z';
                }
            }
        }
    }
    appliquer(graphe, etat_suivant);
}

void appliquer(Graphe *graphe, char *etat_suivant)
{
    for (int i = 0; i < graphe->nb_sommets; i++)
    {
        if (etat_suivant[i] != 'N')
        {
            graphe->successeurs[i]->etat = etat_suivant[i];
        }
    }
    update_successors(graphe);
}

void update_successors(Graphe *graphe)
{
    for (int k = 0; k < graphe->nb_sommets; k++)
    {
        cell *current = (cell *)malloc(sizeof(cell));
        current = graphe->successeurs[k]->suivant;
        while (current != NULL)
        {
            current->etat = graphe->successeurs[current->x * taille + current->y]->etat;
            current = current->suivant;
        }
    }
}
//-------------------------------
//fonctions menus----------------
void Menu(Graphe *graphe, int res[3])
{
    printf("\n1) Modifier les paramètres de propagation");
    printf("\n2) Afficher les paramètres de propagation");
    printf("\n3) Lancer la simulation");
    printf("\n4) Enregistrer les résultats");
    printf("\n5) Quitter");
    printf("\n>");
    int a = 0;
    scanf("%d", &a);
    char name[40];
    if (a == 4)
    {
        printf("\nNom du fichier\n>>");
        scanf("%s", name);
    }
    int data1[] = {taille, duree};
    float data2[] = {pInfectesAtStart, tIncubation, pInfection, pMortalite, pRetablissement, pImmun, pMortVersZombie, pZmortDef};
    int stat[3] = {0, 0, 0};
    int param = 0;
    switch (a)
    {
    case 1:
        printf("\n1)taille de population (côté du carré)\n2)durée de l'expérience\n3)proportion d'infectés à t=0\n4)temps d'incubation\n5)taux d'infection\n6)taux de mortalité\n7)taux de rétablissement\n8)proportion de vaccinés\n9)chance pour un infecté de devenir Zombie à sa mort\n10)chance de mourir définitivement\n>");
        scanf("%d", &param);
        changeParam(param);
        break;
    case 2:
        printf("\ntaille de population (côté du carré) : %d", taille);
        printf("\ndurée de l'expérience : %d\nproportion d'infectés à t=0 : %.2f\ntemps d'incubation : %.2f\ntaux d'infection : %.2f\ntaux de mortalité : %.2f\ntaux de rétablissement : %.2f\nproportion de vaccinés : %.2f\nchance pour un infecté de devenir Zombie à sa mort : %.2f\nchance de mourir définitivement : %.2f\n", duree, pInfectesAtStart, tIncubation, pInfection, pMortalite, pRetablissement, pImmun, pMortVersZombie, pZmortDef);
        break;
    case 3:
        res = letsGo(graphe);
        printf("\nSimulation finie en %d jours\nLe pic d'infectés a eu lieu au jour %d : %d\n", res[0], res[2], res[1]);
        break;
    case 4:
        stat[0] = res[1];
        stat[1] = res[2];
        stat[2] = res[0];
        enregistre_graphe(graphe, name, data1, data2, stat);
        break;
    case 5:
        return;
    default:
        break;
    }
    Menu(graphe, res);
}

void changeParam(int param)
{
    int t = 0;
    float u = 0;
    printf("\nvaleur : ");
    if (param == 1 || param == 2)
        scanf("%d", &t);
    else
    {
        scanf("%f", &u);
    }
    switch (param)
    {
    case 1:
        taille = t;
        break;
    case 2:
        duree = t;
        break;
    case 3:
        pInfectesAtStart = u;
        break;
    case 4:
        tIncubation = u;
        break;
    case 5:
        pInfection = u;
        break;
    case 6:
        pMortalite = u;
        break;
    case 7:
        pRetablissement = u;
        break;
    case 8:
        pImmun = u;
        break;
    case 9:
        pMortVersZombie = u;
        break;
    case 10:
        pZmortDef = u;
        break;
    default:
        break;
    }
}
//-------------------------------
//-------------------------------------------------------------