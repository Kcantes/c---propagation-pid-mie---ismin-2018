//Projet d'AlgoProg 2, par Quentin CHEVALIER et Maxime CHIANESE 06/04/2019
//graphe.c : code source des fonctions de graphe utilisées

#include "graphe.h"

void init_graphe(Graphe *G, int taille, float pImmun)
{
	G->nb_sommets = taille * taille;
	G->successeurs = (cell **)malloc(taille * taille * sizeof(cell *));
	srand(time(NULL));
	for (int i = 0; i < taille; i++)
	{
		for (int j = 0; j < taille; j++)
		{
			cell *s = (cell *)malloc(sizeof(cell));
			s->x = i;
			s->y = j;
			s->tm = 0;
			int immu = rand();
			if (immu <= RAND_MAX * pImmun)
				s->etat = 'V';
			else
				s->etat = 'S';
			G->successeurs[taille * i + j] = s;
		}
	}
	for (int i = 0; i < taille; i++)
	{
		for (int j = 0; j < taille; j++)
		{
			if (i != 0 && i != taille - 1)
			{
				if (j != 0 && j != taille - 1)
				{
					cell *n = (cell *)malloc(sizeof(cell));
					n->x = G->successeurs[taille * (i - 1) + j]->x;
					n->y = G->successeurs[taille * (i - 1) + j]->y;
					n->etat = G->successeurs[taille * (i - 1) + j]->etat;
					G->successeurs[taille * i + j]->suivant = n;
					cell *s = (cell *)malloc(sizeof(cell));
					s->x = G->successeurs[taille * (i + 1) + j]->x;
					s->y = G->successeurs[taille * (i + 1) + j]->y;
					s->etat = G->successeurs[taille * (i + 1) + j]->etat;
					n->suivant = s;
					cell *w = (cell *)malloc(sizeof(cell));
					w->x = G->successeurs[taille * i + j - 1]->x;
					w->y = G->successeurs[taille * i + j - 1]->y;
					w->etat = G->successeurs[taille * i + j - 1]->etat;
					s->suivant = w;
					cell *e = (cell *)malloc(sizeof(cell));
					e->x = G->successeurs[taille * i + j + 1]->x;
					e->y = G->successeurs[taille * i + j + 1]->y;
					e->etat = G->successeurs[taille * i + j + 1]->etat;
					w->suivant = e;
				}
				else if (j != 0)
				{
					cell *n = (cell *)malloc(sizeof(cell));
					n->x = G->successeurs[taille * (i - 1) + j]->x;
					n->y = G->successeurs[taille * (i - 1) + j]->y;
					n->etat = G->successeurs[taille * (i - 1) + j]->etat;
					G->successeurs[taille * i + j]->suivant = n;
					cell *s = (cell *)malloc(sizeof(cell));
					s->x = G->successeurs[taille * (i + 1) + j]->x;
					s->y = G->successeurs[taille * (i + 1) + j]->y;
					s->etat = G->successeurs[taille * (i + 1) + j]->etat;
					n->suivant = s;
					cell *w = (cell *)malloc(sizeof(cell));
					w->x = G->successeurs[taille * i + j - 1]->x;
					w->y = G->successeurs[taille * i + j - 1]->y;
					w->etat = G->successeurs[taille * i + j - 1]->etat;
					s->suivant = w;
				}
				else
				{
					cell *n = (cell *)malloc(sizeof(cell));
					n->x = G->successeurs[taille * (i - 1) + j]->x;
					n->y = G->successeurs[taille * (i - 1) + j]->y;
					n->etat = G->successeurs[taille * (i - 1) + j]->etat;
					G->successeurs[taille * i + j]->suivant = n;
					cell *s = (cell *)malloc(sizeof(cell));
					s->x = G->successeurs[taille * (i + 1) + j]->x;
					s->y = G->successeurs[taille * (i + 1) + j]->y;
					s->etat = G->successeurs[taille * (i + 1) + j]->etat;
					n->suivant = s;
					cell *e = (cell *)malloc(sizeof(cell));
					e->x = G->successeurs[taille * i + j + 1]->x;
					e->y = G->successeurs[taille * i + j + 1]->y;
					e->etat = G->successeurs[taille * i + j + 1]->etat;
					s->suivant = e;
				}
			}
			else if (i != 0)
			{
				if (j != 0 && j != taille - 1)
				{
					cell *n = (cell *)malloc(sizeof(cell));
					n->x = G->successeurs[taille * (i - 1) + j]->x;
					n->y = G->successeurs[taille * (i - 1) + j]->y;
					n->etat = G->successeurs[taille * (i - 1) + j]->etat;
					G->successeurs[taille * i + j]->suivant = n;
					cell *w = (cell *)malloc(sizeof(cell));
					w->x = G->successeurs[taille * i + j - 1]->x;
					w->y = G->successeurs[taille * i + j - 1]->y;
					w->etat = G->successeurs[taille * i + j - 1]->etat;
					n->suivant = w;
					cell *e = (cell *)malloc(sizeof(cell));
					e->x = G->successeurs[taille * i + j + 1]->x;
					e->y = G->successeurs[taille * i + j + 1]->y;
					e->etat = G->successeurs[taille * i + j + 1]->etat;
					w->suivant = e;
				}
				else if (j != 0)
				{
					cell *n = (cell *)malloc(sizeof(cell));
					n->x = G->successeurs[taille * (i - 1) + j]->x;
					n->y = G->successeurs[taille * (i - 1) + j]->y;
					n->etat = G->successeurs[taille * (i - 1) + j]->etat;
					G->successeurs[taille * i + j]->suivant = n;
					cell *w = (cell *)malloc(sizeof(cell));
					w->x = G->successeurs[taille * i + j - 1]->x;
					w->y = G->successeurs[taille * i + j - 1]->y;
					w->etat = G->successeurs[taille * i + j - 1]->etat;
					n->suivant = w;
				}
				else
				{
					cell *n = (cell *)malloc(sizeof(cell));
					n->x = G->successeurs[taille * (i - 1) + j]->x;
					n->y = G->successeurs[taille * (i - 1) + j]->y;
					n->etat = G->successeurs[taille * (i - 1) + j]->etat;
					G->successeurs[taille * i + j]->suivant = n;
					cell *e = (cell *)malloc(sizeof(cell));
					e->x = G->successeurs[taille * i + j + 1]->x;
					e->y = G->successeurs[taille * i + j + 1]->y;
					e->etat = G->successeurs[taille * i + j + 1]->etat;
					n->suivant = e;
				}
			}
			else
			{
				if (j != 0 && j != taille - 1)
				{
					cell *s = (cell *)malloc(sizeof(cell));
					s->x = G->successeurs[taille * (i + 1) + j]->x;
					s->y = G->successeurs[taille * (i + 1) + j]->y;
					s->etat = G->successeurs[taille * (i + 1) + j]->etat;
					G->successeurs[taille * i + j]->suivant = s;
					cell *w = (cell *)malloc(sizeof(cell));
					w->x = G->successeurs[taille * i + j - 1]->x;
					w->y = G->successeurs[taille * i + j - 1]->y;
					w->etat = G->successeurs[taille * i + j - 1]->etat;
					s->suivant = w;
					cell *e = (cell *)malloc(sizeof(cell));
					e->x = G->successeurs[taille * i + j + 1]->x;
					e->y = G->successeurs[taille * i + j + 1]->y;
					e->etat = G->successeurs[taille * i + j + 1]->etat;
					w->suivant = e;
				}
				else if (j != 0)
				{
					cell *s = (cell *)malloc(sizeof(cell));
					s->x = G->successeurs[taille * (i + 1) + j]->x;
					s->y = G->successeurs[taille * (i + 1) + j]->y;
					s->etat = G->successeurs[taille * (i + 1) + j]->etat;
					G->successeurs[taille * i + j]->suivant = s;
					cell *w = (cell *)malloc(sizeof(cell));
					w->x = G->successeurs[taille * i + j - 1]->x;
					w->y = G->successeurs[taille * i + j - 1]->y;
					w->etat = G->successeurs[taille * i + j - 1]->etat;
					s->suivant = w;
				}
				else
				{
					cell *s = (cell *)malloc(sizeof(cell));
					s->x = G->successeurs[taille * (i + 1) + j]->x;
					s->y = G->successeurs[taille * (i + 1) + j]->y;
					s->etat = G->successeurs[taille * (i + 1) + j]->etat;
					G->successeurs[taille * i + j]->suivant = s;
					cell *e = (cell *)malloc(sizeof(cell));
					e->x = G->successeurs[taille * i + j + 1]->x;
					e->y = G->successeurs[taille * i + j + 1]->y;
					e->etat = G->successeurs[taille * i + j + 1]->etat;
					s->suivant = e;
				}
			}
		}
	}
}

void affiche_graphe(Graphe *G)
{
	cell *courant;
	for (int i = 0; i < G->nb_sommets; i++)
	{
		printf("Case n°%d %dx%d (%c) : ", i, G->successeurs[i]->x, G->successeurs[i]->y, G->successeurs[i]->etat);
		courant = G->successeurs[i]->suivant;
		while (courant != NULL)
		{
			printf("%dx%d (%c), ", courant->x, courant->y, courant->etat);
			courant = courant->suivant;
		}
		printf("\n");
	}
}

int recensement(Graphe *G)
{
	int etats[6] = {0, 0, 0, 0, 0, 0};
	for (int i = 0; i < G->nb_sommets; i++)
	{
		if (G->successeurs[i]->etat == 'S')
			etats[0]++;
		else if (G->successeurs[i]->etat == 'V')
			etats[1]++;
		else if (G->successeurs[i]->etat == 'C')
			etats[2]++;
		else if (G->successeurs[i]->etat == 'I')
			etats[3]++;
		else if (G->successeurs[i]->etat == 'M')
			etats[4]++;
		else if (G->successeurs[i]->etat == 'Z')
			etats[5]++;
	}
	printf("Sains : %d\nImmunises : %d\nContamines : %d\nMalades : %d\nMorts : %d\nZombies : %d\n", etats[0], etats[1], etats[2], etats[3], etats[4], etats[5]);
	return (etats[3] + etats[2] + etats[5]);
}

void pos_to_cell(Graphe *G, int taille, cell *c, int x, int y)
{
	if (x < 0 || x >= taille || y < 0 || y >= taille)
		return;
	c = G->successeurs[taille * x + y];
}

void affiche_graphe2(Graphe *G, int taille)

{
	for (int i = 0; i < taille; i++)
	{
		for (int j = 0; j < taille; j++)
		{
			printf("%c ", G->successeurs[10 * i + j]->etat);
		}
		printf("\n");
	}
}

void enregistre_graphe(Graphe *G, char *nom_fichier, int data1[2], float data2[8], int stat[3])

{
	FILE *fichier;
	fichier = fopen(nom_fichier, "w+");
	if (fichier != NULL)
	{
		fprintf(fichier, "Paramètres de la simulation :\n");
		fprintf(fichier, "Taille de population (côté du carré) : %d\nDurée de l'expérience : %d\nProportion d'infectés à t=0 : %.2f\nTemps d'incubation : %.2f\nTaux d'infection : %.2f\nTaux de mortalité : %.2f\nTaux de rétablissement : %.2f\nProportion de vaccinés : %.2f\nChance pour un mort de devenir Zombie : %.2f\nChance de mourir définitivement : %.2f\n\n", data1[0], data1[1], data2[0], data2[1], data2[2], data2[3], data2[4], data2[5], data2[6], data2[7]);

		fprintf(fichier, "Matrice finale :\n");
		for (int i = 0; i < data1[0]; i++)
		{
			for (int j = 0; j < data1[0]; j++)
			{
				fprintf(fichier, "%c ", G->successeurs[10 * i + j]->etat);
			}
			fprintf(fichier, "\n");
		}
		fprintf(fichier, "\n");
		int etats[6] = {0, 0, 0, 0, 0, 0};
		for (int i = 0; i < G->nb_sommets; i++)
		{
			if (G->successeurs[i]->etat == 'S')
				etats[0]++;
			else if (G->successeurs[i]->etat == 'V')
				etats[1]++;
			else if (G->successeurs[i]->etat == 'C')
				etats[2]++;
			else if (G->successeurs[i]->etat == 'I')
				etats[3]++;
			else if (G->successeurs[i]->etat == 'M')
				etats[4]++;
			else if (G->successeurs[i]->etat == 'Z')
				etats[5]++;
		}
		fprintf(fichier, "Sains : %d\nImmunises : %d\nContamines : %d\nMalades : %d\nMorts : %d\nZombies : %d\n", etats[0], etats[1], etats[2], etats[3], etats[4], etats[5]);
		fprintf(fichier, "\nSimulation terminée en %d jours, avec un pic d'infectés au jour %d : %d infectés.\n", stat[2], stat[1], stat[0]);
		fclose(fichier);
	}
}
