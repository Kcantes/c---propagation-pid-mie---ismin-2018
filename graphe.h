//Projet d'AlgoProg 2, par Quentin CHEVALIER et Maxime CHIANESE 06/04/2019
//graphe.h : header des structures et fonctions de graphe utilisées

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct _cell
{
	int x;
	int y;
	char etat;
	int tm;//incubation
	struct _cell *suivant;
} cell;

typedef struct
{
	int nb_sommets;
	cell **successeurs;
} Graphe;

void init_graphe(Graphe *G, int taille, float pImmun);//initialise le graphe de côté taille, avec une proportion d'immunisés pImmun
void affiche_graphe(Graphe *G);//Affiche le graphe conventionnellement noeud : arêtes
void affiche_graphe2(Graphe *G, int taille);//Affiche le graphe sous forme d'une grille
void enregistre_graphe(Graphe *G, char *nom_fichier, int data1[2], float data2[8], int stat[3]);//Enregistre le tableau de bord
int recensement(Graphe *G);//Recense le nombre de sains, infectés etc, renvoie infectés + contaminés + zombies
void pos_to_cell(Graphe *G, int taille, cell *c, int x, int y);//inutilisée